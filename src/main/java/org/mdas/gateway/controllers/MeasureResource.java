package org.mdas.gateway.controllers;

import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.mdas.gateway.model.Measure;
import org.mdas.gateway.streams.Producer;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * MeasureResource takes measures from sensors
 */
@Path("/measure")
@Consumes(MediaType.APPLICATION_JSON)
public class MeasureResource {
    @Inject
    Producer producer;

    Float highestTemperature = 0f;

    /**
     * produceMeasure gets a Measure by POST and produces a message with it
     * 
     * @param measure A Measure object, should go to the measures topic, serialized
     *                to JSON.
     * @return An HTTP Response object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "measureCreated", description = "How many measures have been created.")
    @Timed(name = "checksTimer", description = "A measure of how long it takes to perform the "
            + "operation.", unit = MetricUnits.MILLISECONDS)
    public Response produceMeasure(Measure measure) {
        this.producer.produceMeasure(measure);
        // To have an updated metric
        if (measure.getTemperature() > this.highestTemperature) {
            this.highestTemperature = measure.getTemperature();
        }

        return Response.ok().entity("SUCCESS").build();
    }

    @Gauge(name = "highestTemperature", unit = MetricUnits.NONE, description = "Highest "
            + "Temperature number so far.")
    public Float highestPrimeNumberSoFar() {
        return highestTemperature;
    }
}

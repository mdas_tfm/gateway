package org.mdas.gateway.streams;

import java.util.HashMap;
import java.util.Map;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;

/**
 * Use the in-memory connector to avoid having to use a real broker.
 */
public class FakeKafkaResource implements QuarkusTestResourceLifecycleManager {

    @Override
    public Map<String, String> start() {
        Map<String, String> env = new HashMap<>();
        Map<String, String> measures = InMemoryConnector.switchOutgoingChannelsToInMemory("measure-topic");
        Map<String, String> sensors = InMemoryConnector.switchOutgoingChannelsToInMemory("sensor-topic");
        env.putAll(measures);
        env.putAll(sensors);
        return env;
    }

    @Override
    public void stop() {
        InMemoryConnector.clear();
    }
}

package org.mdas.gateway.controllers;

import javax.enterprise.inject.Any;
import javax.inject.Inject;
import static io.restassured.RestAssured.given;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mdas.gateway.model.Measure;
import org.mdas.gateway.model.MeasureObjectMother;
import org.mdas.gateway.streams.FakeKafkaResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.reactive.messaging.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.connectors.InMemorySink;

/**
 * Testing the Measure intake resource using FakeKafkaResource
 * (in-memory-connector)
 */
@QuarkusTest
@QuarkusTestResource(FakeKafkaResource.class)
public class MeasureResourceTest {

    @Inject
    @Any
    InMemoryConnector connector;

    @AfterEach
    public void clearSink() {
        InMemorySink<Integer> results = connector.sink("measure-topic");
        results.clear();
    }

    @Test
    public void shouldReturnOkWithMeasureData() {
        Measure newMeasure = MeasureObjectMother.measureFromMontcada();
        InMemorySink<Integer> results = connector.sink("measure-topic");

        given().header("Content-Type", "application/json").body(newMeasure).when().post("/measure").then()
                .statusCode(200);

        Assertions.assertEquals(1, results.received().size());
    }

    @Test
    public void shouldReturnBadRequestWithWrongData() {
        String wrongData = "This is not measure data";
        InMemorySink<Integer> results = connector.sink("measure-topic");

        given().header("Content-Type", "application/json").body(wrongData).when().post("/measure").then()
                .statusCode(400);

        Assertions.assertEquals(0, results.received().size());
    }

    @Test
    public void shouldReturnUnsupportedMediaTypeWithNoContentType() {
        Measure newMeasure = MeasureObjectMother.measureFromMontcada();
        InMemorySink<Integer> results = connector.sink("measure-topic");

        given().body(newMeasure).when().post("/measure").then().statusCode(415);

        Assertions.assertEquals(0, results.received().size());
    }
}

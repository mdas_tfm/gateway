package org.mdas.gateway.model;

public class LocationObjectMother {
    public static Location montcada() {
        return new Location(41.493662, 2.187147);
    }
}

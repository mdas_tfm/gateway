package org.mdas.gateway.model;

public class MeasureObjectMother {
    public static Measure measureFromMontcada() {
        return new Measure(29.5f, "2020-08-09 17:26", SensorObjectMother.montcada());
    }
}

package org.mdas.gateway.model;

public class SensorObjectMother {
    public static Sensor montcada() {
        return new Sensor("S1", "Montcada", LocationObjectMother.montcada());

    }
}

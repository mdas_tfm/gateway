# gateway project

[![pipeline status](https://gitlab.com/mdas_tfm/gateway/badges/master/pipeline.svg)](https://gitlab.com/mdas_tfm/gateway/-/commits/master)

Measures gateway project, transports measures from sensors to Kafka.

## Running the application in dev mode

### Dev mode configuration

* Using the *application.properties*, use the prefix *%dev.\<property>*, as in:

  `%dev.kafka.bootstrap.servers=192.168.99.100:32100`

**NOTE**: The default profile (no prefix) should be left only for kubernetes

* Environment variables take precedence over the *application.properties* file, for instance the kafka bootstrap servers could be set as follows:

  ```shell
  export KAFKA_BOOTSTRAP_SERVERS=172.17.0.2:321000
  ```

### Running the application

You can run your application in dev mode that enables live coding using:

```shell
./gradlew quarkusDev
```

## Packaging and running the application

### Versioning

This project uses the Gradle plugin `com.palantir.git-version` to version the artifacts according to git, tags plus current commit, on a clean repository it should only be the last tag.

### Build the JAR

The application can be packaged using `./gradlew quarkusBuild`.
It produces the `gateway-1.0.0-SNAPSHOT-runner.jar` file in the `build` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `build/lib` directory.

The application is now runnable using `java -jar build/gateway-1.0.0-SNAPSHOT-runner.jar`.

If you want to build an _über-jar_, just add the `--uber-jar` option to the command line:

```shell
./gradlew quarkusBuild --uber-jar
```

### Building and pushing a container image

This project uses Gitlab's container image registry by default, the image can be built and pushed as follows:

```shell
docker login -u username  -p deployment_token registry.gitlab.com
./gradlew quarkusBuild -Dquarkus.container-image.build=true -Dquarkus.container-image.push=true
```

### Deploy it in kubernetes

It requires kafka to be running on the *streams* namespace, as done by default in our automation.

After building and pushing the container image, execute the following commands:

```shell
$ kubectl create ns tfm
namespace/tfm
$ kubectl apply -f build/kubernetes/kubernetes.json -n tfm
serviceaccount/gateway created
service/gateway created
rolebinding.rbac.authorization.k8s.io/gateway:view created
deployment.apps/gateway created
ingress.extensions/gateway created
$ kubectl get pods -n tfm
tfm           gateway-854f7dcbc7-ztr8p                          0/1     ContainerCreating   0          0s
tfm           gateway-854f7dcbc7-ztr8p                          0/1     Running             0          4s
tfm           gateway-854f7dcbc7-ztr8p                          1/1     Running             0          45s
```

## Send a new measure

```shell
curl --location --request POST 'http://localhost:8080/measure' \
--header 'Content-Type: application/json' \
--data-raw '    {
      "metadata": {
        "location": {
          "latitude": 88,
          "longitude": 281.999
        },
        "name": "MONTCADA"
      },
      "takenAt": "2020-05-31T14:59:02Z",
      "temperature": 22.209919609068038
    }'
```
